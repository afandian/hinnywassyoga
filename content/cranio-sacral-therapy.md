---
title: "About Cranio-Sacral Therapy"
menu: main
weight: 20
---

# About Cranio-Sacral therapy

## What is Cranio-Sacral Therapy? What happens during a treatment?

Cranio-Sacral Therapy is a very gentle and profound form of bodywork. In CST the practitioner uses caring, respectful touch to listen carefully to the body, enabling it to release tension and pain, both physical and emotional.

During a cranio-sacral treatment you remain fully clothed, and you lie down or sit in a comfortable position, supported with pillows and warm under a blanket, if needed. Hinny will make gentle contact with your body, on top of clothing and blankets, always checking in with you about each new hand position to make sure that you feel as relaxed as possible. During the session you can talk and move as much or as little as you wish, and the treatment can still be effective even if you fall asleep.

Because it is so gentle, CST is suitable for people of all ages, from newborns to the very eldery, and everyone in between.

Sessions last for an hour, though they may be shorter for young children and infants. During that time you can talk about how you’re doing, Hinny will take a case-history or follow-up notes, and then the hands-on treatment will begin.

## What can Cranio-Sacral Therapy help with?

Cranial treatment has been found to help people find relief from many conditions, including chronic muscle tension, headaches, anxiety, depression, asthma, vaginismus, colic, torticollis, reflux, back pain, shoulder pain, neck pain, hip pain, tight jaw, teeth grinding, chronic pain after dentistry, long term effects of surgery, stress and overwhelm, dissociation, pelvic floor issues, joint pain, glue ear, chronic congestion, digestive problems, disrupted sleep, chronic fatigue, and many, many more.

Following a cranio-sacral treatment, people often report feeling more relaxed, calmer, happier, lighter, more comfortable, and more like themselves, and with a noticeable reduction in symptoms.

[Click here for details of where Hinny practises, how much sessions cost, and how to book](/cst-locations-and-prices).