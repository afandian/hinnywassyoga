---
title: Sanctuary Yoga
menu: main
weight: 40
---

# Sanctuary Yoga

Sanctuary Yoga is a radically kind form of yoga, through which you can develop a more gentle and accepting relationship with your body.  The practice enables you to find safety in the sanctuary of your body, and to find comfortable movement and breath.  It is a blend of breathwork, asanas and sequences from the Forrest Yoga tradition, combined with practices drawn from Cranio-Sacral Therapy and other mindful somatic practices.  During the session you will be guided through poses which stretch, poses which strengthen, poses which aid rest and nervous-system downregulation, and poses which offer manageable challenge.  You will have the chance to let go of the limiting tensions, thoughts and patterns which you have unconsciously been holding onto. 
There are two main aims in a Sanctuary Yoga class: first, through tracking sensation you can get a clearer picture of where your body, mind and heart are currently at.  This unlocks the second: now you can decide how to respond to the communications from your body and move towards what you desire for your body, your mind and your heart.

Through a regular practice of Sanctuary Yoga you can expect to become more comfortable and stable in your body, more relaxed and resilient physically and emotionally, and to feel more like yourself.



### Group Classes

| Day | Venue | Description | Price |
|-----|-------|-------------|-------|
| Mondays 6.30pm to 8.00pm | South Oxford Community Centre  | All welcome! <br> [Click here to book](https://docs.google.com/forms/d/e/1FAIpQLSckMBTe0I56Ix0qRuwX91LJVLfVb7zYwO7kG33xcd2s6oMi1A/viewform) | £13 <br> (£8 unwaged)|
|-------------|-------------|-------------|-------------|



If you like the class and want to come regularly, then you can buy a six-class card for £65, to be used within six months.

Our room in the community centre is the Gill Garrat Room, next to the Dojo. Access via side door or through the courtyard at the back.

If you’re pregnant, or think you might be, then drop Hinny an email before booking to check that the class will be suitable for you.


### Private Classes and Yoga Therapy

#### What happens in a one-to-one session?

In a one-to-one session Hinny will guide you through yoga breathing techniques and poses in a compassionate and exploratory way, creating a sequence which is tailored to your unique needs. Through listening carefully to the body, we can work together to help your body and mind let go of that which no longer serves you, leaving you happier, healthier, and more comfortable. Hinny’s mindful approach is equally effective, regardless of whether the discomfort you’re working with is physical or emotional. The process is always supported, caring, agency-filled and respectful.

#### Who is it for?

Everyone! Anyone is welcome to book an individual session. Working in this way may be particularly beneficial for someone who:
 - wants to practise yoga in a private and completely supported environment
 - has persistent aches and pains (especially if your current yoga practice helps ease them temporarily but they keep returning)
 - is returning to yoga after a break/ an injury/ surgery/ pregnancy
 - wants to explore healing emotional distress or mental health issues through yoga
 - has suffered trauma
 - wants help to achieve a particular goal in their yoga practice, for example a particular pose, or greater flexibility

#### Yoga and Cranio-Sacral Therapy
Hinny is also a qualified Cranio-Sacral Therapist, and, with your permission, may blend in elements of cranial work into the yoga session where appropriate. For more information about Cranio-Sacral Therapy, visit my CST page.

#### How to Book
To book a session, or for more information, email hinny@hinnywassyoga.co.uk.

