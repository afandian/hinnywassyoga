---
title: About Hinny
menu: main
weight: 50
---

# About Hinny

<img src="/head.jpg" style="width: 20rem; float: left; margin: 2rem;">



Hinny discovered Forrest Yoga whilst searching for a remedy for the aches and pains inherent in life as a professional fiddle player.  She was lucky enough to attend classes led by Jambo Truong and Conrad Freese, which not only helped immensely with the pain in her neck and shoulders but which also helped her feel better in many other ways, and get stronger than she’d ever thought possible.  In 2016 (having just completed an MA in linguistics, specialising in syntax and semantics) Hinny trained to become a Forrest Yoga teacher, with Cat Allen and Heidi Sormaz.  Since then she has been teaching group classes and one-to-one yoga sessions in Oxford.  Recently Hinny has developed a subtly powerful new form of yoga, Sanctuary Yoga, which is a blend of Forrest Yoga together with elements from Cranio-Sacral Therapy and other mindful somatic practices.  

Hinny first discovered the profound healing of Cranio-Sacral Therapy in the skillful hands of Emily Carson, whilst seeking help with pelvic issues post-pregnancy in 2019, and swiftly decided to train to become a Cranio-Sacral Therapist.  It felt like a natural step into CST from Forrest Yoga.  Hinny trained at the College of Cranio-Sacral Therapy in London, under Thomas Attlee, and graduated in the summer of 2022.  She is a member of [the Cranio-Sacral Therapy Association](https://www.craniosacral.co.uk/).  

Other significant influences on Hinny’s approach are the works of Tara Brach and Brené Brown on Radical Acceptance and shame, the writings of Gabor Mate, Bessel van der Kolk and Peter Levine on the impact and processing of trauma, and Rob Kelly’s Thrive Programme.  Hinny’s work is also enriched and informed by a lifetime of her own healing, from chronic fatigue, emetophobia, perfectionism and social anxiety, amongst other things.  She has an embodied understanding not only that healing is possible, but also how to help it happen.  

Hinny feels immense gratitude every day to be working in the wonderful modalities of CST and yoga, helping people feel better.  She would be delighted to help you feel better too!



hinny@hinnywassyoga.co.uk
