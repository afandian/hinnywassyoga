---
title: "Home"
date: "2014-04-09"
menu: main
layout: "about"
weight: 10
---

<div class="row">
<div class="col-md-3">
<img src="/head.jpg" class="img-fluid">
</div>
<div class="col-md-9">
<h1>Hinny Wass</h1>
Cranio-Sacral Therapy and Sanctuary Yoga in Oxford
</div>
</div>


{{< startcols >}}


<img class="img-fluid" src="/noun_1994214.svg" style="width: 10em">

## [Cranio-Sacral Therapy](/cranio-sacral-therapy)

Hinny is a qualified practitioner of Cranio-Sacral Therapy, a very gentle and profound form of bodywork, which can help give you relief from pain, tension, emotional discomfort, chronic fatigue, anxiety, depression, and much more. 


{{< col >}}



<img class="img-fluid" src="/noun_255181.svg" style="width: 10em" >

## [Sanctuary Yoga](/sanctuary-yoga)


Sanctuary Yoga is a radically kind form of yoga, through which you can develop a more gentle and accepting relationship with your body.  The practice enables you to find safety in the sanctuary of your body, and to find comfortable movement and breath.  It is a blend of Forrest Yoga with elements of Cranio-Sacral Therapy and other mindful somatic practices.


{{< endcols >}}

<!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
</style>
<div id="mc_embed_signup">
<form action="https://hinnywassyoga.us15.list-manage.com/subscribe/post?u=45c2205f4f4f80c4dcb75c9fd&amp;id=f1b473003b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div id="mc_embed_signup_scroll">
<h2>Subscribe to Hinny's mailing list</h2>
<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
<div class="mc-field-group">
<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group">
<label for="mce-FNAME">First Name </label>
<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
</div>
<div class="mc-field-group">
<label for="mce-LNAME">Last Name </label>
<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>
<div id="mce-responses" class="clear">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_45c2205f4f4f80c4dcb75c9fd_f1b473003b" tabindex="-1" value=""></div>
<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
</div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->



   
   
