---
title: "Cranio-Sacral Therapy Locations and Prices"
menu: main
weight: 30
---

# Cranio-Sacral Therapy Locations and Prices

Hinny provides Cranio-Sacral treatment at her home in East Oxford, near Donnington Bridge, in a dedicated, quiet, comfortable, warm, sunny space at the end of the garden.

Hinny's full fee is £60 for an hour's session.  

To book, email hinny@hinnywassyoga.co.uk

## Home Visits

By arrangement.

£60 per hour. 

Includes up to 30 minutes travel time free of charge.  If you live more than a 15 minute drive from Oxford then additional travel time is charged at £60 per hour.

To book, email hinny@hinnywassyoga.co.uk

## Reduced Fees

If the full fee is beyond your budget then reductions may be possible.  Contact hinny@hinnywassyoga.co.uk to discuss.

